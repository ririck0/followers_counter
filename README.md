# FollowersCounter

### PROJET:
- Installation du core Arduino ESP8266 (2.5):
Ajouter dans "Aditional Board Manager URL: "http://arduino.esp8266.com/stable/package_esp8266com_index.json"
- BOARD testées:
	- WeMos D1 R1
	- LOLIN(WeMos) D1 mini

- Librairies utilisées (intégrées au projet):
	- "ArduinoJson" par bblanchon dans une version < 6 (détail plus bas dans la description) : https://github.com/bblanchon/ArduinoJson/releases/tag/v5.13.2
	- "json-streaming-parser" par squix78 : https://github.com/squix78/json-streaming-parser
	- "arduino-youtube-api" par witnessmenow : https://github.com/witnessmenow/arduino-youtube-api
	- "arduino-facebook-api" par witnessmenow : https://github.com/witnessmenow/arduino-facebook-api
	- "arduino-instagram-stats" par witnessmenow : https://github.com/witnessmenow/arduino-instagram-stats
	- "Adafruit_NeoPixel" par adafruit : https://github.com/adafruit/Adafruit_NeoPixel

### HARD:
- Les LEDS sont pilotées vie la pin D4
- Le reset de l'eeprom est provoqué par une mise à la masse de la pin D7
- Le test de l'intégralité des LEDS est provoqué par une mise à la masse de la pin D1
- Les logs de debug sont envoyé sur l'UART de DEBUG lors que la pin D3 est reliée à la masse
- Pour ignorer le mode dégradé (voir section associée), il faut relier la pin D2 à la masse

### CONFIGURATION INITIALE:
- Les paramètres wifi/api configurables se trouvent config.h
- Pour remplacer les pin utiliser, il faut éditer le fichier *followers_config.h"

### MODE DEGRADÉE:
- Lorsque qu'une ou plusieurs LEDs sont cramées, il se produits un décalage des leds pilotées. Pour y remedier, il faut éditer la structure "g_led_state" dans *followers_counter.h* et indiquer les LEDS cramé en replaçant le "1" par un "0".
- Il est possible de forcer le mode non dégradé en reliant la pin D2 à la masse

### CONFIGURATION EN FONCTIONNEMENT:
Une fois le programme démarré, on peut accéder aux paramètres en se connectant à l'ESP8266 grâce à son adresse IP ainsi qu'aux identifiants "CONFIG_WEBSERVER" indiqués dans le fichier "config.h".
L'adresse IP est affichée sur le panneau de LEDs dès que l'ESP se connecte à un réseau internet.

### OTA Update:
Une fois le programme flashé une première fois dans l'ESP8266, les mises à jour peuvent être effectuées à travers le réseau (OTA) grâce au mot de passe spécifié dans le fichier "config.h".

### API SETTINGS:
Pour Twitter et Instagram, seul le nom d'utilisateur suffit, pas besoin de clé d'API.
Pour récupérer les clés d'API Youtube et Facebook:
* Youtube : https://console.developers.google.com/apis
* Facebook : https://developers.facebook.com/
	* Obtenir le token facebook: Se rendre sur: "https://developers.facebook.com/tools/explorer"
		- Cliquer sur "Obtenir le token", puis "Obtenir un token d'accès utilisateur"
		- Cocher "manage_pages" et "pages_show_list" puis cliquer sur "Obtenir un token d'accès"
		- Copier le token généré, puis se rendre sur "https://developers.facebook.com/tools/debug/accesstoken"
		- Coller le token puis cliquer sur Débuguer (pour générer un token ralongé d'env 2 mois) => En bas de la page cliquer sur Débuguer.
		- Le token est à récupérer sur cette page.
		- Pour vérifier son bon fonctionnement:
Dans GET: coller "me/accounts?fields=fan_count" puis envoyer. Dans la console devrait apparaitre le nombre de "like" de la page

### WARNING:
- Pourquoi faut-il une version du core Arduino ESP8266 dans une version différente de 2.4.1 ?
=> Car cette version contient une fuite de mémoire.
Le problème a été corrigé dans la version 2.4.2, attention donc à bien être à jour.
- Pourquoi faut-il une version de "ArduinoJson" dans une version inférieure à 6 ?
=> Car à partir de la version 6, l'api a complêtement changée. Ce projet a été codé avec une version 5.x
