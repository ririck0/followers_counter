// Wifi settings
#define CONFIG_WIFI_SSID_1 ""
#define CONFIG_WIFI_PASS_1 ""
#define CONFIG_WIFI_SSID_2 ""
#define CONFIG_WIFI_PASS_2 ""
#define CONFIG_WIFI_SSID_3 ""
#define CONFIG_WIFI_PASS_3 ""
#define CONFIG_WIFI_SSID_4 ""
#define CONFIG_WIFI_PASS_4 ""
#define CONFIG_WIFI_SSID_5 ""
#define CONFIG_WIFI_PASS_5 ""

// OTA update settings
#define CONFIG_OTA_PASS "followerscounter"

// Web server settings
#define CONFIG_WERSERVER_USER "admin"
#define CONFIG_SERVERWEB_PASS "pass"

// Facebook settings
#define CONFIG_FACEBOOK_ENABLE_BY_DEFAULT true
#define CONFIG_FACEBOOK_MAX_CALL_PER_HOUR 120
#define CONFIG_FACEBOOK_PAGE_ID "xxx"
#define CONFIG_FACEBOOK_APP_ID "xxx"
#define CONFIG_FACEBOOK_APP_SECRET "xxx"
#define CONFIG_FACEBOOK_ACCESS_TOKEN "xxx"

// Instagram settings
#define CONFIG_INSTAGRAM_ENABLE_BY_DEFAULT true
#define CONFIG_INSTAGRAM_MAX_CALL_PER_HOUR 240
#define CONFIG_INSTAGRAM_PAGE_NAME "xxx"

// Twitter settings
#define CONFIG_TWITTER_ENABLE_BY_DEFAULT true
#define CONFIG_TWITTER_MAX_CALL_PER_HOUR 240
#define CONFIG_TWITTER_PAGE_NAME "xxx"

// YouTube settings
#define CONFIG_YOUTUBE_ENABLE_BY_DEFAULT true
#define CONFIG_YOUTUBE_MAX_CALL_PER_HOUR 120
#define CONFIG_YOUTUBE_CHANNEL_ID "xxx"
#define CONFIG_YOUTUBE_API_KEY "xxx"
