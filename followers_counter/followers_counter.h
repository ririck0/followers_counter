#define VERSION "V01.02"

#define RESET_PIN           D7
#define TEST_LED_PIN        D1
#define FORCE_ALL_LED_MODE  D2
#define DEBUG_PIN           D3
#define LED_PIN             D4

#define LED_NBR                           320
#define LED_BRIGHTNESS_INITIAL_PERCENTAGE 20
#define PANNEL_HEIGHT                     8
#define PANNEL_WIDTH                      40
#define LOGO_WITDH                        10
#define TEXT_WITDH                        29
#define MEDIA_DURATION_VALUE_SEC          4
#define REFRESH_INTERVAL_MS               2000

typedef enum {
	MEDIA_FACEBOOK  = 0,
	MEDIA_INSTAGRAM = 1,
	MEDIA_TWITTER   = 2,
	MEDIA_YOUTUBE   = 3,
	MEDIA_COUNT, /* Must be the last one */
} E_media;

typedef struct {
	/* Facebook */
	char fb_page_id[32];
	char fb_app_id[32];
	char fb_app_secret[64];
	char fb_access_token[256];
	/* Instagram */
	char insta_page_name[32];
	/* Twitter */
	char tw_page_name[32];
	/* Youtube */
	char yt_channel_id[32];
	char yt_api_key[64];
} T_api_info;

typedef struct {
	bool initial_status;
	const String name;
	uint8_t display_duration_sec;
	unsigned int limit_call_per_hour;
} T_media_config;

typedef struct {
	unsigned long last_call_ms;
	unsigned long last_value;
	bool first_call_done;
	bool media_enabled;
	bool in_error;
	bool obsolete_value;
} T_media_value;

static struct {
	ESP8266WebServer * server;
	WiFiClientSecure * client;
	ESP8266WiFiMulti * wifiMulti;
	Adafruit_NeoPixel * band;
	FacebookApi * facebookApi;
	YoutubeApi * youtubeApi;
	InstagramStats * instaStats;

	byte led_brightness;
	byte previous_media;
	unsigned long previous_number;
	T_media_config media_config[MEDIA_COUNT];
	T_media_value media_value[MEDIA_COUNT];
	byte pannel[PANNEL_HEIGHT][PANNEL_WIDTH][3];
	T_api_info api_info;
} g_followers_counter = {
	.server = NULL,
	.client = NULL,
	.wifiMulti = NULL,
	.band = NULL,
	.facebookApi = NULL,
	.youtubeApi = NULL,
	.instaStats = NULL,
	.led_brightness = LED_BRIGHTNESS_INITIAL_PERCENTAGE,
	.previous_media = 0,
	.previous_number = 0,
	.media_config = {
		//MEDIA_FACEBOOK
		{
			.initial_status = CONFIG_FACEBOOK_ENABLE_BY_DEFAULT,
			.name = "Facebook",
			.display_duration_sec = MEDIA_DURATION_VALUE_SEC,
			.limit_call_per_hour = CONFIG_FACEBOOK_MAX_CALL_PER_HOUR,
		},
		//MEDIA_INSTAGRAM
		{
			.initial_status = CONFIG_INSTAGRAM_ENABLE_BY_DEFAULT,
			.name = "Instagram",
			.display_duration_sec = MEDIA_DURATION_VALUE_SEC,
			.limit_call_per_hour = CONFIG_INSTAGRAM_MAX_CALL_PER_HOUR,
		},
		//MEDIA_TWITTER
		{
			.initial_status = CONFIG_TWITTER_ENABLE_BY_DEFAULT,
			.name = "Twitter",
			.display_duration_sec = MEDIA_DURATION_VALUE_SEC,
			.limit_call_per_hour = CONFIG_TWITTER_MAX_CALL_PER_HOUR,
		},
		//MEDIA_YOUTUBE
		{
			.initial_status = CONFIG_YOUTUBE_ENABLE_BY_DEFAULT,
			.name = "YouTube",
			.display_duration_sec = MEDIA_DURATION_VALUE_SEC,
			.limit_call_per_hour = CONFIG_YOUTUBE_MAX_CALL_PER_HOUR,
		},
	},
};

/* EEPROM CONTENT:
 * +-----------+------+--------------------------------------+
 * |     @     | SIZE + CONTENT                              |
 * +-----------+------+--------------------------------------+
 * |     0     |   1  + 0x42                                 |
 * +-----------+------+--------------------------------------+
 * |     1     |   1  + brightness [0 - 100]                 |
 * +-----------+------+--------------------------------------+
 * |     2     |   1  + STATUS_FACEBOOK [ 0 -1]              |
 * +-----------+------+--------------------------------------+
 * |     3     |   1  + DELAY_FACEBOOK [ 0 - 255]            |
 * +-----------+------+--------------------------------------+
 * |     4     |   1  + STATUS_INSTAGRAM [ 0 -1]             |
 * +-----------+------+--------------------------------------+
 * |     5     |   1  + DELAY_INSTAGRAM [ 0 - 255]           |
 * +-----------+------+--------------------------------------+
 * |     6     |   1  + STATUS_TWITTER [ 0 -1]               |
 * +-----------+------+--------------------------------------+
 * |     7     |   1  + DELAY_TWITTER [ 0 - 255]             |
 * +-----------+------+--------------------------------------+
 * |     8     |   1  + STATUS_YOUTUBE [ 0 -1]               |
 * +-----------+------+--------------------------------------+
 * |     9     |   1  + DELAY_YOUTUBE [ 0 - 255]             |
 * +-----------+------+--------------------------------------+
 * |  10 - 41  |  32  + FB PAGE ID                           |
 * +-----------+------+--------------------------------------+
 * |  42 - 73  |  32  + FB APP ID                            |
 * +-----------+------+--------------------------------------+
 * |  74 - 137 |  64  + FB APP SECRET                        |
 * +-----------+------+--------------------------------------+
 * | 138 - 393 | 256  + FB SECRET TOKEN                      |
 * +-----------+------+--------------------------------------+
 * | 394 - 425 |  32  + TWITTER PAGE NAME                    |
 * +-----------+------+--------------------------------------+
 * | 426 - 457 |  32  + INSTAGRAM PAGE NAME                  |
 * +-----------+------+--------------------------------------+
 * | 458 - 489 |  32  + YOUTUBE CHANNEL ID                   |
 * +-----------+------+--------------------------------------+
 * | 490 - 553 |  32  + YOUTUBE API KEYU                     |
 * +-----------+------+--------------------------------------+
 * |    554    |   1  + 0x69                                 |
 * +-----------+------+--------------------------------------+
 */
#define EEPROM_CHECK_VALUE 0x42
#define EEPROM_END_VALUE 0x69
#define EEPROM_SIZE (1 + 1 + (MEDIA_COUNT*2) + sizeof(T_api_info) + 1)
#define EEPROM_API_SETTINGS_OFFSET (1 + 1 + MEDIA_COUNT*2)
#define RTC_MEMORY_START (EEPROM_SIZE+1)
#define RTC_MEMORY_SIZE  (sizeof( unsigned long) * MEDIA_COUNT)

/* Set KO led status to 0 to avoid/patch shift */
const bool g_led_state[PANNEL_HEIGHT][PANNEL_WIDTH] = {
	{ 1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,},
	{ 1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,},
	{ 1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,},
	{ 1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,},
	{ 1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,},
	{ 1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,},
	{ 1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,},
	{ 1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,   1,1,1,1,1,1,1,1,},
};

const bool g_numbers[10][PANNEL_HEIGHT][4] = {
	{
		{ 0, 0, 0, 0},
		{ 1, 1, 1, 1},
		{ 1, 0, 0, 1},
		{ 1, 0, 0, 1},
		{ 1, 0, 0, 1},
		{ 1, 0, 0, 1},
		{ 1, 0, 0, 1},
		{ 1, 1, 1, 1},
	},
	{
		{ 0, 0, 0, 0},
		{ 0, 0, 0, 1},
		{ 0, 0, 0, 1},
		{ 0, 0, 0, 1},
		{ 0, 0, 0, 1},
		{ 0, 0, 0, 1},
		{ 0, 0, 0, 1},
		{ 0, 0, 0, 1},
	},
	{
		{ 0, 0, 0, 0},
		{ 1, 1, 1, 1},
		{ 0, 0, 0, 1},
		{ 0, 0, 0, 1},
		{ 1, 1, 1, 1},
		{ 1, 0, 0, 0},
		{ 1, 0, 0, 0},
		{ 1, 1, 1, 1},
	},
	{
		{ 0, 0, 0, 0},
		{ 1, 1, 1, 1},
		{ 0, 0, 0, 1},
		{ 0, 0, 0, 1},
		{ 1, 1, 1, 1},
		{ 0, 0, 0, 1},
		{ 0, 0, 0, 1},
		{ 1, 1, 1, 1},
	},
	{
		{ 0, 0, 0, 0},
		{ 1, 0, 0, 1},
		{ 1, 0, 0, 1},
		{ 1, 0, 0, 1},
		{ 1, 1, 1, 1},
		{ 0, 0, 0, 1},
		{ 0, 0, 0, 1},
		{ 0, 0, 0, 1},
	},
	{
		{ 0, 0, 0, 0},
		{ 1, 1, 1, 1},
		{ 1, 0, 0, 0},
		{ 1, 0, 0, 0},
		{ 1, 1, 1, 1},
		{ 0, 0, 0, 1},
		{ 0, 0, 0, 1},
		{ 1, 1, 1, 1},
	},
	{
		{ 0, 0, 0, 0},
		{ 1, 1, 1, 1},
		{ 1, 0, 0, 0},
		{ 1, 0, 0, 0},
		{ 1, 1, 1, 1},
		{ 1, 0, 0, 1},
		{ 1, 0, 0, 1},
		{ 1, 1, 1, 1},
	},
	{
		{ 0, 0, 0, 0},
		{ 1, 1, 1, 1},
		{ 0, 0, 0, 1},
		{ 0, 0, 0, 1},
		{ 0, 0, 0, 1},
		{ 0, 0, 0, 1},
		{ 0, 0, 0, 1},
		{ 0, 0, 0, 1},
	},
	{
		{ 0, 0, 0, 0},
		{ 1, 1, 1, 1},
		{ 1, 0, 0, 1},
		{ 1, 0, 0, 1},
		{ 1, 1, 1, 1},
		{ 1, 0, 0, 1},
		{ 1, 0, 0, 1},
		{ 1, 1, 1, 1},
	},
	{
		{ 0, 0, 0, 0},
		{ 1, 1, 1, 1},
		{ 1, 0, 0, 1},
		{ 1, 0, 0, 1},
		{ 1, 1, 1, 1},
		{ 0, 0, 0, 1},
		{ 0, 0, 0, 1},
		{ 1, 1, 1, 1},
	},
};

const bool g_error_string[PANNEL_HEIGHT][TEXT_WITDH] = {
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{ 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0},
	{ 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1},
	{ 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1},
	{ 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0},
	{ 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0},
	{ 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0},
	{ 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1},
};

const bool g_wifi_string[PANNEL_HEIGHT][TEXT_WITDH] = {
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
	{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
	{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
	{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
	{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
	{ 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
	{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
};

const bool g_no_media_string[PANNEL_HEIGHT][TEXT_WITDH] = {
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{ 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0},
	{ 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0},
	{ 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0},
	{ 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0},
	{ 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0},
	{ 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0},
	{ 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0},
};

const byte g_yt_logo_colors[3][3] = {{0, 0, 0}, {255, 0, 0}, {255, 255, 255}};
const byte g_yt_logo[PANNEL_HEIGHT][LOGO_WITDH] = {
	{0, 1, 1, 1, 1, 1, 1, 1, 1, 0},
	{1, 1, 1, 1, 2, 1, 1, 1, 1, 1},
	{1, 1, 1, 1, 2, 2, 1, 1, 1, 1},
	{1, 1, 1, 1, 2, 2, 2, 1, 1, 1},
	{1, 1, 1, 1, 2, 2, 2, 1, 1, 1},
	{1, 1, 1, 1, 2, 2, 1, 1, 1, 1},
	{1, 1, 1, 1, 2, 1, 1, 1, 1, 1},
	{0, 1, 1, 1, 1, 1, 1, 1, 1, 0}
};

const byte g_fb_logo_colors[3][3] = {{0, 0, 0}, {40, 40, 125}, {255, 255, 255}};
const byte g_fb_logo[PANNEL_HEIGHT][LOGO_WITDH] = {
	{0, 1, 1, 1, 1, 2, 2, 2, 1, 0},
	{1, 1, 1, 1, 2, 2, 2, 2, 1, 1},
	{1, 1, 1, 1, 2, 2, 1, 1, 1, 1},
	{1, 1, 1, 1, 2, 2, 1, 1, 1, 1},
	{1, 1, 1, 2, 2, 2, 2, 2, 1, 1},
	{1, 1, 1, 1, 2, 2, 1, 1, 1, 1},
	{1, 1, 1, 1, 2, 2, 1, 1, 1, 1},
	{0, 1, 1, 1, 2, 2, 1, 1, 1, 0}
};

const byte g_tw_logo_colors[3][3] = {{0, 0, 0}, {40, 70, 125}, {255, 255, 255}};
const byte g_tw_logo[PANNEL_HEIGHT][LOGO_WITDH] = {
	{0, 1, 1, 2, 2, 1, 1, 1, 1, 0},
	{1, 1, 1, 2, 2, 1, 1, 1, 1, 1},
	{1, 1, 1, 2, 2, 2, 2, 2, 1, 1},
	{1, 1, 1, 2, 2, 2, 2, 2, 1, 1},
	{1, 1, 1, 2, 2, 1, 1, 1, 1, 1},
	{1, 1, 1, 2, 2, 1, 1, 1, 1, 1},
	{1, 1, 1, 2, 2, 2, 2, 2, 1, 1},
	{0, 1, 1, 1, 2, 2, 2, 2, 1, 0}
};

const byte g_insta_logo_colors[8][3] = {{0, 0, 0}, {255, 255, 255}, {255, 190, 0}, {205, 80, 0}, {80, 10, 10}, {175, 0, 170}, {45, 0, 85}, {40, 20, 180}};
const byte g_insta_logo[PANNEL_HEIGHT][LOGO_WITDH] = {
	{0, 5, 1, 1, 1, 1, 1, 1, 7, 0},
	{4, 1, 5, 5, 6, 6, 6, 1, 1, 7},
	{4, 1, 4, 5, 1, 1, 6, 6, 1, 7},
	{4, 1, 4, 1, 4, 6, 1, 6, 1, 7},
	{3, 1, 3, 1, 4, 5, 1, 6, 1, 7},
	{3, 1, 3, 3, 1, 1, 4, 5, 1, 6},
	{2, 1, 2, 2, 3, 3, 4, 4, 1, 5},
	{0, 2, 1, 1, 1, 1, 1, 1, 4, 0}
};

const byte g_error_logo_colors[3][3] = {{0, 0, 0}, {248, 0, 0}, {255, 255, 255}};
const byte g_error_logo[PANNEL_HEIGHT][LOGO_WITDH] = {
	{0, 1, 1, 1, 1, 1, 1, 1, 1, 0},
	{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
	{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
	{1, 1, 2, 2, 2, 2, 2, 2, 1, 1},
	{1, 1, 2, 2, 2, 2, 2, 2, 1, 1},
	{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
	{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
	{0, 1, 1, 1, 1, 1, 1, 1, 1, 0}
};

const byte g_wifi_logo_colors[3][3] = {{0, 0, 0}, {0, 171, 255}};
const byte g_wifi_logo[PANNEL_HEIGHT][10] = {
	{0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
	{0, 1, 0, 0, 0, 0, 0, 0, 1, 0},
	{1, 0, 0, 1, 1, 1, 1, 0, 0, 1},
	{0, 0, 1, 0, 0, 0, 0, 1, 0, 0},
	{0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
	{0, 0, 0, 1, 0, 0, 1, 0, 0, 0},
	{0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
	{0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
};
