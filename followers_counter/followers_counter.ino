#include <Esp.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include <JsonStreamingParser.h>
#include <YoutubeApi.h>
#include <FacebookApi.h>
#include <InstagramStats.h>
#include <Adafruit_NeoPixel.h>
#include "config.h"
#include "followers_counter.h"

#define debug_printf(...) if( digitalRead( DEBUG_PIN) == 0) printf( __VA_ARGS__);
#define debug_print(string) if( digitalRead( DEBUG_PIN) == 0) Serial.print( F(string));
#define debug_println(string) if( digitalRead( DEBUG_PIN) == 0) Serial.println( F(string));

static void _reboot( void) {
	ESP.restart();

	while(1) {
		wdt_reset();
	}
	return;
}

static bool _debugging( void) {
	return (digitalRead( DEBUG_PIN) == 0);
}

static bool _test_all_leds( void) {
	return (digitalRead( TEST_LED_PIN) == 0);
}

static bool _degraded_mode_broken_leds( void) {
	return (digitalRead( FORCE_ALL_LED_MODE) == 0);
}

void _perf_monitor( void) {
	static uint32_t time = 0;

	if( _debugging() == false) {
		return;
	}

	if( millis() - time > 5000) {
		debug_log( "[PERF MONITOR] Heap: %5d - Frag: %3d%% - FreeBS: %5d\n", ESP.getFreeHeap(), ESP.getHeapFragmentation(), ESP.getMaxFreeBlockSize());
		time = millis();
	}
}

void _pin_monitor( void) {
	static uint32_t time = 0;

	if( millis() - time > 5000) {
		debug_printf( "[PIN MONITOR] RESET %d - TEST_LED %d - FORCE_LED %d - DEBUG %d\n", digitalRead( RESET_PIN), digitalRead( TEST_LED_PIN), digitalRead( FORCE_ALL_LED_MODE), digitalRead( DEBUG_PIN));
		time = millis();
	}
}

void _write_last_value_to_rtc( int media) {
	if( media < 0 || media >= MEDIA_COUNT) {
		Serial.print( "Error BAD MEDIA !!!!!!! ");
		Serial.println( media);
		return;
	}
	system_rtc_mem_write( (RTC_MEMORY_START + (media * sizeof( g_followers_counter.media_value[media].last_value))), &g_followers_counter.media_value[media].last_value, sizeof( g_followers_counter.media_value[media].last_value));
}

void _read_last_values_from_rtc( void) {
	if( g_followers_counter.unexpected_restart == false) {
		return;
	}

	unsigned long saved_values[MEDIA_COUNT];
	int media;
	debug_println( "## READ LAST VALUES FROM RTC ##");
	for( int media = 0; media < MEDIA_COUNT; media++) {
		if( g_followers_counter.media_value[media].media_enabled == false) {
			continue;
		}
		system_rtc_mem_read( (RTC_MEMORY_START + (media * sizeof( g_followers_counter.media_value[media].last_value))), &g_followers_counter.media_value[media].last_value, sizeof( g_followers_counter.media_value[media].last_value));
		g_followers_counter.media_value[media].first_call_done = true;
		debug_printf( "media %d => %d\n", media, g_followers_counter.media_value[media].last_value);
	}
	return;
}


void _led_keep_alive( void) {
	if( _debugging() == false) {
		return;
	}

	g_followers_counter.band->setPixelColorWithBrightness( 0 * 64 + 0 * 8 + 0, 0, 255, 0, 2);
	g_followers_counter.band->show();
	delay(50);
	g_followers_counter.band->setPixelColor( 0 * 64 + 0 * 8 + 0, 0, 0, 0);
	g_followers_counter.band->show();
}

void _refresh_display( void) {
	int p, i, j;
	int offset = 0;

	/* Test All LED */
	if( _test_all_leds() == true) {
		debug_println( "TEST ALL LED MODE");
		for( p = 0; p < PANNEL_WIDTH / 8; p++) {
			for( i = 0; i < 8; i++) {
				for( j = 0; j < 8; j++) {
					g_followers_counter.band->setPixelColor( p * 64 + i * 8 + j, 255,255,255);
				}
			}
		}
		g_followers_counter.band->show();
		return;
	}

	for( p = 0; p < PANNEL_WIDTH / 8; p++) {
		for( i = 0; i < 8; i++) {
			for( j = 0; j < 8; j++) {
				if( g_led_state[i][((p * 8) + j)] == 0) {
					if( _degraded_mode_broken_leds() == false) {
						offset++;
						continue;
					} else {
						debug_printf( "ALL LED MODE: LED [%d][%d] forced\n", i, ((p * 8) + j));
						offset = 0;
					}
				}
				g_followers_counter.band->setPixelColor( p * 64 + i * 8 + j - offset, g_followers_counter.pannel[i][p * 8 + j][0], g_followers_counter.pannel[i][p * 8 + j][1], g_followers_counter.pannel[i][p * 8 + j][2]);
			}
		}
	}

	g_followers_counter.band->show();
}

int _get_digit( unsigned long number, int index) {
	for( int i = 0; i < index - 1; i++) {
		number /= 10;
	}

	return number % 10;
}

void _set_area_color( int x_start, int x_end, int y_start, int y_end, char r_value, char g_value, char b_value) {
	for( int i = y_start; i <= y_end; i++) {
		for( int j = x_start; j <= x_end; j++) {
			g_followers_counter.pannel[i][j][0] = r_value;
			g_followers_counter.pannel[i][j][1] = g_value;
			g_followers_counter.pannel[i][j][2] = b_value;
		}
	}
}

void _print_digit( char digit, int x_pos, int y_pos, char r_value, char g_value, char b_value, int shift = 0) {
	/* Erase on bad digit */
	if( digit < 0 || digit > 9) {
		for( int y = 0; y < 8 - abs(shift); y++) {
			for( int x = -1; x < 4; x++) {
				g_followers_counter.pannel[y_pos + y][x_pos + x][0] = 0;
				g_followers_counter.pannel[y_pos + y][x_pos + x][1] = 0;
				g_followers_counter.pannel[y_pos + y][x_pos + x][2] = 0;
			}
		}
		return ;
	}

	for( int y = 0; y < 8 - abs(shift); y++) {
		/* Erase First column */
		g_followers_counter.pannel[y_pos + y][x_pos - 1][0] = 0;
		g_followers_counter.pannel[y_pos + y][x_pos - 1][1] = 0;
		g_followers_counter.pannel[y_pos + y][x_pos - 1][2] = 0;
		for( int x = 0; x < 4; x++) {
			if(x_pos + x < PANNEL_WIDTH && y_pos + y < PANNEL_HEIGHT) {
				g_followers_counter.pannel[y_pos + y][x_pos + x][0] = (r_value * g_numbers[digit][y][x]);
				g_followers_counter.pannel[y_pos + y][x_pos + x][1] = (g_value * g_numbers[digit][y][x]);
				g_followers_counter.pannel[y_pos + y][x_pos + x][2] = (b_value * g_numbers[digit][y][x]);
			}
		}
	}
}

void _print_6_digits_number( unsigned long number, int x_pos, int y_pos, char r_value, char g_value, char b_value) {
	for( int i = 0; i < 6; i++) {
		_print_digit( _get_digit( number, 6 - i), x_pos + i * 5, y_pos, r_value, g_value, b_value);
	}
	_refresh_display();
}

void _print_6_digits_number_with_animation( unsigned long number, int x_pos, int y_pos, char r_value, char g_value, char b_value, int in_out_anim = 0) {
	float start_number;
	float step_delta;

	if( g_followers_counter.previous_number < 0) {
		g_followers_counter.previous_number = 0;
	}
	start_number = g_followers_counter.previous_number;
	step_delta = ((float) number - g_followers_counter.previous_number) / 3.0;

	for( int anim_step = 0; anim_step < (in_out_anim == 0 ? 3 : 1); anim_step++) {
		if( in_out_anim == 0) {
			number = round(((float) start_number) + step_delta * (anim_step + 1));
		}

		//for(int a = 7; a >= 0; a--)
		for( int a = 0; a < 8; a++) {
			_set_area_color( x_pos, x_pos + 28, y_pos, y_pos + 7, 0, 0, 0);

			for( int i = 0; i < 6; i++) {
				int new_digit = _get_digit( number, 6 - i);
				int previous_digit = _get_digit( g_followers_counter.previous_number, 6 - i);
				int delta = new_digit - previous_digit;
				bool up_shift = step_delta > 0;

				if( delta == 0 && in_out_anim == 0) {
					_print_digit( new_digit, x_pos + i * 5, y_pos, r_value, g_value, b_value);
				} else {
					if( in_out_anim != 0) {
						up_shift = false;
					}

					int previous_digit_shift = up_shift ? a : -a;
					int new_digit_shift = up_shift ? a - 7 : 7 - a;

					int previous_digit_y_pos = up_shift ? y_pos : y_pos + a + 2;
					int new_digit_y_pos = up_shift ? y_pos + 7 - a : y_pos;

					if( in_out_anim != 1) {
						_print_digit( previous_digit, x_pos + i * 5, previous_digit_y_pos, r_value, g_value, b_value, previous_digit_shift);
					}

					if( in_out_anim != -1) {
						_print_digit( new_digit, x_pos + i * 5, new_digit_y_pos, r_value, g_value, b_value, new_digit_shift);
					}
				}
			}

			_refresh_display();

			delay(50);
		}

		g_followers_counter.previous_number = number;
	}
}

void _print_string( const bool string[8][29]) {
	_set_area_color( 11, PANNEL_WIDTH-1, 0, PANNEL_HEIGHT-1, 0, 0, 0);

	for( int x = 11; x < PANNEL_WIDTH; x++) {
		for( int y = 0; y < PANNEL_HEIGHT; y++) {
			if( string[y][x-11] == 1) {
				g_followers_counter.pannel[y][x][0] = 255;
				g_followers_counter.pannel[y][x][1] = 255;
				g_followers_counter.pannel[y][x][2] = 255;
			}
		}
	}

	_refresh_display();
}

void debug_serial_print_pannel() {
	for( int i = 0; i < PANNEL_HEIGHT; i++) {
		for( int j = 0; j < PANNEL_WIDTH; j++) {
			Serial.print( String(g_followers_counter.pannel[i][j][0]));
			Serial.print( F("\t"));
			Serial.print( String(g_followers_counter.pannel[i][j][1]));
			Serial.print( F("\t"));
			Serial.print( String(g_followers_counter.pannel[i][j][2]));
			Serial.print( F("\t\t"));
		}
		Serial.println( F(""));
	}
}

void _print_logo( const byte logo[][10], const byte colors[][3], int animation = 4) {
	for( int i = 0; i < 8; i++) {
		for( int j = 4 - animation; j < 6 + animation; j++) {
			for( int k = 0; k < 3; k++) {
				g_followers_counter.pannel[i][j][k] = colors[logo[i][j]][k];
			}
		}
	}
}

void _print_logo_with_animation( const byte logo[][10], const byte colors[][3]) {
	for( int i = 0; i < 5; i++) {
		_print_logo( logo, colors, i);
		_refresh_display();
		delay(50);
	}
}

String _send_get( String url, String ssl_certificate_footprint = "") {
	HTTPClient http;

	debug_printf( "command: %s\n", url.c_str());
	if( ssl_certificate_footprint == "") {
		http.begin( url);
	}
	else {
		http.begin( url, ssl_certificate_footprint);
	}

	int httpCode = http.GET();

	String returnValue;

	returnValue = httpCode > 0 ? http.getString() : "Error : no HTTP code";

	http.end();;
	return returnValue;
}

int _get_twitter_follower_count( String profile_id) {
	String answer = _send_get( "http://cdn.syndication.twimg.com/widgets/followbutton/info.json?screen_names=" + profile_id);
	debug_printf( "response: %s\n", answer.c_str());

	// On enlève les crochets [] qui entourent le json et le rendent illisible par le parser
	answer.remove( 0, 1);
	answer.remove( answer.length() - 1, 1);

	DynamicJsonBuffer json_buffer;
	JsonObject& root = json_buffer.parseObject(answer);

	if( root.success()) {
		if( root.containsKey("followers_count")) {
			return root["followers_count"].as<int>();
		}

		Serial.println( F("Incompatible JSON"));
	} else {
		Serial.println( F("Failed to parse JSON"));
	}

	return -1;
}

int _get_instagram_follower_count( String page_name)
{
	g_followers_counter.instaStats->_debug = _debugging();
	InstagramUserStats response = g_followers_counter.instaStats->getUserStats( page_name);
	return response.followedByCount;
}

int _get_youtube_subscriber_count( String channel_id) {
	if( g_followers_counter.youtubeApi->getChannelStatistics( channel_id, _debugging())) {
		return g_followers_counter.youtubeApi->channelStats.subscriberCount;
	} else {
		return -1;
	}
}

int _get_facebook_fan_count( String page_id) {
	return g_followers_counter.facebookApi->getPageFanCount( page_id, _debugging());
}

void _print_media_settings( void) {
	debug_println("");
	for( int media = 0; media < MEDIA_COUNT; media++) {
		debug_printf( "%-9s - Enabled: %d - Duration: %d\n", g_followers_counter.media_config[media].name.c_str(), g_followers_counter.media_value[media].media_enabled, g_followers_counter.media_config[media].display_duration_sec);
	}
	debug_println("");
}

void _read_api_settings_from_eeprom( void) {
	uint8_t buffer[sizeof(T_api_info)];
	size_t api_offset = 0;

	for( int i = 0; i < sizeof( buffer); i++) {
		buffer[i] = EEPROM.read(EEPROM_API_SETTINGS_OFFSET+i);
	}

	memcpy( g_followers_counter.api_info.fb_page_id, &buffer[api_offset], sizeof( g_followers_counter.api_info.fb_page_id));
	api_offset += sizeof( g_followers_counter.api_info.fb_page_id);
	memcpy( g_followers_counter.api_info.fb_app_id, &buffer[api_offset], sizeof( g_followers_counter.api_info.fb_app_id));
	api_offset += sizeof( g_followers_counter.api_info.fb_app_id);
	memcpy( g_followers_counter.api_info.fb_app_secret, &buffer[api_offset], sizeof( g_followers_counter.api_info.fb_app_secret));
	api_offset += sizeof( g_followers_counter.api_info.fb_app_secret);
	memcpy( g_followers_counter.api_info.fb_access_token, &buffer[api_offset], sizeof( g_followers_counter.api_info.fb_access_token));
	api_offset += sizeof( g_followers_counter.api_info.fb_access_token);

	memcpy( g_followers_counter.api_info.insta_page_name, &buffer[api_offset], sizeof( g_followers_counter.api_info.insta_page_name));
	api_offset += sizeof( g_followers_counter.api_info.insta_page_name);

	memcpy( g_followers_counter.api_info.tw_page_name, &buffer[api_offset], sizeof( g_followers_counter.api_info.tw_page_name));
	api_offset += sizeof( g_followers_counter.api_info.tw_page_name);

	memcpy( g_followers_counter.api_info.yt_channel_id, &buffer[api_offset], sizeof( g_followers_counter.api_info.yt_channel_id));
	api_offset += sizeof( g_followers_counter.api_info.yt_channel_id);
	memcpy( g_followers_counter.api_info.yt_api_key, &buffer[api_offset], sizeof( g_followers_counter.api_info.yt_api_key));
	api_offset += sizeof( g_followers_counter.api_info.yt_api_key);
}

void _print_api_settings( void) {
	if( _debugging() == false) {
		return;
	}
	memset( &g_followers_counter.api_info, '\0', sizeof( g_followers_counter.api_info));
	_read_api_settings_from_eeprom();

	debug_printf( "Facebook page ID:      %s\n", (char *) g_followers_counter.api_info.fb_page_id);
	debug_printf( "Facebook app ID:       %s\n", (char *) g_followers_counter.api_info.fb_app_id);
	debug_printf( "Facebook app secret:   %s\n", (char *) g_followers_counter.api_info.fb_app_secret);
	debug_printf( "Facebook access token: %s\n", (char *) g_followers_counter.api_info.fb_access_token);
	debug_printf( "Instagram page name:   %s\n", (char *) g_followers_counter.api_info.insta_page_name);
	debug_printf( "Twitter page name      %s\n", (char *) g_followers_counter.api_info.tw_page_name);
	debug_printf( "Youtube channel ID:    %s\n", (char *) g_followers_counter.api_info.yt_channel_id);
	debug_printf( "Youtuve API key:       %s\n", (char *) g_followers_counter.api_info.yt_api_key);
}

void _read_settings_from_eeprom( void) {
	if( EEPROM.read(0) != EEPROM_CHECK_VALUE) {
		Serial.println( F("Bad EEPROM format. Settings read abort."));
		return;
	} else {
		g_followers_counter.led_brightness = EEPROM.read(1);

		for( int media = 0; media < MEDIA_COUNT; media++) {
			int offset = 2 + media*2;
			g_followers_counter.media_value[media].media_enabled = EEPROM.read( offset);
			g_followers_counter.media_config[media].display_duration_sec = EEPROM.read( offset + 1);
		}

		_read_api_settings_from_eeprom();
	}
}

void _write_media_settings_to_eeprom( int media) {
	int offset = 2 + media*2;

	EEPROM.write( offset, g_followers_counter.media_value[media].media_enabled);
	EEPROM.write( offset+1, g_followers_counter.media_config[media].display_duration_sec);
	EEPROM.commit();
}

void _write_brightness_setting_to_eeprom( void) {
	EEPROM.write( 1, g_followers_counter.led_brightness);
	EEPROM.commit();
}

void _write_api_settings_to_eeprom( void) {
	size_t api_offset = 0;
	uint8_t buffer[sizeof(T_api_info)];

	memcpy( &buffer[api_offset], g_followers_counter.api_info.fb_page_id, sizeof( g_followers_counter.api_info.fb_page_id));
	api_offset += sizeof( g_followers_counter.api_info.fb_page_id);
	memcpy( &buffer[api_offset], g_followers_counter.api_info.fb_app_id, sizeof( g_followers_counter.api_info.fb_app_id));
	api_offset += sizeof( g_followers_counter.api_info.fb_app_id);
	memcpy( &buffer[api_offset], g_followers_counter.api_info.fb_app_secret, sizeof( g_followers_counter.api_info.fb_app_secret));
	api_offset += sizeof( g_followers_counter.api_info.fb_app_secret);
	memcpy( &buffer[api_offset], g_followers_counter.api_info.fb_access_token, sizeof( g_followers_counter.api_info.fb_access_token));
	api_offset += sizeof( g_followers_counter.api_info.fb_access_token);

	memcpy( &buffer[api_offset], g_followers_counter.api_info.insta_page_name, sizeof( g_followers_counter.api_info.insta_page_name));
	api_offset += sizeof( g_followers_counter.api_info.insta_page_name);

	memcpy( &buffer[api_offset], g_followers_counter.api_info.tw_page_name, sizeof( g_followers_counter.api_info.tw_page_name));
	api_offset += sizeof( g_followers_counter.api_info.tw_page_name);

	memcpy( &buffer[api_offset], g_followers_counter.api_info.yt_channel_id, sizeof( g_followers_counter.api_info.yt_channel_id));
	api_offset += sizeof( g_followers_counter.api_info.yt_channel_id);
	memcpy( &buffer[api_offset], g_followers_counter.api_info.yt_api_key, sizeof( g_followers_counter.api_info.yt_api_key));
	api_offset += sizeof( g_followers_counter.api_info.yt_api_key);

	for( int i = 0; i < sizeof( buffer); i++) {
		EEPROM.write( EEPROM_API_SETTINGS_OFFSET+i, buffer[i]);
	}
	EEPROM.commit();

	_print_api_settings();
}

void _write_settings_to_eeprom( void) {
	for( int media = 0; media < MEDIA_COUNT; media++) {
		_write_media_settings_to_eeprom( media);
	}

	_write_brightness_setting_to_eeprom();
	_write_api_settings_to_eeprom();

	EEPROM.write( 0, EEPROM_CHECK_VALUE);
	EEPROM.write( (EEPROM_SIZE-1), EEPROM_END_VALUE);

	EEPROM.commit();
}

String _generate_index_html( String message) {
	String html;
	debug_println( "Regenerate html page");

	html.reserve(5000);

	html  = F("<!DOCTYPE html>");
	html += F("<html>");
	html += F("<head>");
	html += F("<meta charset=\"UTF-8\">");
	html += F("<title>Followers Counter</title>");
	//html += "<style>input[type=submit]{ padding: 3px;  } input[type=\"text\"]{ width:200px;margin: 0 0 0 0;padding: 2px 5px;} input[type=\"number\"]{ width:40px;}td,th {border: 1px solid rgb(190, 190, 190);padding: 10px;}td {text-align: center;}tr:nth-child(even) {background-color: #eee;}th[scope=\"col\"] {background-color: #696969;color: #fff;}th[scope=\"row\"] {background-color: #d7d9f2;}caption {padding: 10px; caption-side: bottom; } table {border-collapse: collapse;border: 2px solid rgb(200, 200, 200); letter-spacing: 1px; font-family: sans-serif; font-size: .8rem;    }    </style>";
	html += F("</head>");

	html += F("<body>");
	html += F(""); html += message;
	html += F("<br/>");

	html += F("<form action=\"/index\" method=\"get\">");
	html += F("<table class=\"table\">");
	html += F("<tr>");
	html += F("<td>Brightness (%) </td>");
	html += F("<td><input type=\"number\" min=\"0\" max=\"100\" name=\"brightness\" value=\""); html += String(g_followers_counter.led_brightness); html += F("\"></td>");
	html += F("<td><input type=\"submit\" value=\"Apply\"></td>");
	html += F("</tr>");
	html += F("</table>");
	html += F("</form>");
	html += F("<br/>");

	html += F("<form action=\"/index\" method=\"get\">");
	html += F("<input type=\"hidden\" name=\"media_updated\" value=\"true\">");
	html += F("<table class=\"table\">");
	html += F("<tr>");
	html += F("<td>Media</td>");
	html += F("<td>Enabled</td>");
	html += F("<td>Duration</td>");
	html += F("</tr>");
	for( int media = 0; media < MEDIA_COUNT; media++) {
		html += F("<tr>");
		html += F("<td>"); html += g_followers_counter.media_config[media].name; html += F("</td>");
		html += F("<td><input type=\"hidden\" name=\"enabled_"); html += g_followers_counter.media_config[media].name; html += F("\" value=\""); html += String(g_followers_counter.media_value[media].media_enabled ? F("true") : F("false")); html += F("\" checked><input type=\"checkbox\" onclick=\"this.previousSibling.value = !(this.previousSibling.value == 'true')\""); html += String(g_followers_counter.media_value[media].media_enabled ? F("checked") : F("")); html += F("></td>");
		html += F("<td><input type=\"number\" min=\"1\" max=\"255\"name=\"duration_"); html += g_followers_counter.media_config[media].name; html += F("\" value=\""); html += String(g_followers_counter.media_config[media].display_duration_sec); html += F("\"></td>");
		html += F("</tr>");
		html += F("<tr>");
	}
	html += F("</table>");
	html += F("<input type=\"submit\" value=\"Apply\">");
	html += F("<br/>");
	html += F("</form>");
	html += F("<br/>");

	html += F("<form action=\"/index\" method=\"get\">");
	html += F("<table class=\"table\">");
	html += F("<tr>");
	html += F("<td><label for=\"api_settings\">Facebook page ID</label></td>");
	html += F("<td><input type=\"text\" minlength=\"0\" maxlength=\""); html += String(sizeof( g_followers_counter.api_info.fb_page_id)); html += F("\" id=\"fb_page_id\" name=\"fb_page_id\" value=\""); html += String(g_followers_counter.api_info.fb_page_id); html += F("\"></td>");
	html += F("</tr>");
	html += F("<tr>");
	html += F("<td><label for=\"api_settings\">Facebook app ID</label></td>");
	html += F("<td><input type=\"text\" minlength=\"0\" maxlength=\""); html += String(sizeof( g_followers_counter.api_info.fb_app_id)); html += F("\" id=\"fb_app_id\"name=\"fb_app_id\" value=\""); html += String(g_followers_counter.api_info.fb_app_id); html += F("\"></td>");
	html += F("</tr>");
	html += F("<tr>");
	html += F("<td><label for=\"api_settings\">Facebook app secret</label></td>");
	html += F("<td><input type=\"text\" minlength=\"0\" maxlength=\""); html += String(sizeof( g_followers_counter.api_info.fb_app_secret)); html += F("\" id=\"fb_app_secret\"name=\"fb_app_secret\" value=\""); html += String(g_followers_counter.api_info.fb_app_secret); html += F("\"></td>");
	html += F("</tr>");
	html += F("<tr>");
	html += F("<td><label for=\"api_settings\">Facebook access token</label></td>");
	html += F("<td><input type=\"text\" minlength=\"0\" maxlength=\""); html += String(sizeof( g_followers_counter.api_info.fb_access_token)); html += F("\" id=\"fb_access_token\"name=\"fb_access_token\" value=\""); html += String(g_followers_counter.api_info.fb_access_token); html += F("\"></td>");
	html += F("</tr>");
	html += F("</table>");
	html += F("<input type=\"submit\" value=\"Apply and reboot\">");
	html += F("</form>");
	html += F("<br/>");

	html += F("<form action=\"/index\" method=\"get\">");
	html += F("<table class=\"table\">");
	html += F("<tr>");
	html += F("<td><label for=\"api_settings\">Instagram page name</label></td>");
	html += F("<td><input type=\"text\" minlength=\"0\" maxlength=\""); html += String(sizeof( g_followers_counter.api_info.insta_page_name)); html += F("\" id=\"insta_page_name\" name=\"insta_page_name\" value=\""); html += String(g_followers_counter.api_info.insta_page_name); html += F("\"></td>");
	html += F("</table>");
	html += F("<input type=\"submit\" value=\"Apply and reboot\">");
	html += F("</form>");
	html += F("<br/>");

	html += F("<form action=\"/index\" method=\"get\">");
	html += F("<table class=\"table\">");
	html += F("<tr>");
	html += F("<td><label for=\"api_settings\">Twitter page name</label></td>");
	html += F("<td><input type=\"text\" minlength=\"0\" maxlength=\""); html += String(sizeof( g_followers_counter.api_info.tw_page_name)); html += F("\" id=\"tw_page_name\" name=\"tw_page_name\" value=\""); html += String(g_followers_counter.api_info.tw_page_name); html += F("\"></td>");
	html += F("</tr>");
	html += F("</table>");
	html += F("<input type=\"submit\" value=\"Apply and reboot\">");
	html += F("</form>");
	html += F("<br/>");

	html += F("<form action=\"/index\" method=\"get\">");
	html += F("<table class=\"table\">");
	html += F("<tr>");
	html += F("<td><label for=\"api_settings\">Youtube channel ID</label></td>");
	html += F("<td><input type=\"text\" minlength=\"0\" maxlength=\""); html += String(sizeof( g_followers_counter.api_info.yt_channel_id)); html += F("\" id=\"yt_channel_id\" name=\"yt_channel_id\" value=\""); html += String(g_followers_counter.api_info.yt_channel_id); html += F("\"></td>");
	html += F("</tr>");
	html += F("<tr>");
	html += F("<td><label for=\"api_settings\">Youtube API key</label></td>");
	html += F("<td><input type=\"text\" minlength=\"0\" maxlength=\""); html += String(sizeof( g_followers_counter.api_info.yt_api_key)); html += F("\" id=\"yt_api_key\" name=\"yt_api_key\" value=\""); html += String(g_followers_counter.api_info.yt_api_key); html += F("\"></td>");
	html += F("</table>");
	html += F("<input type=\"submit\" value=\"Apply and reboot\">");
	html += F("</form>");
	html += F("<br/>");

	html += F("<button onClick=\"window.location = window.location.pathname\">Refresh</button>");
	html += F("</body>");
	html += F("</html>");

	debug_println( "html page generated");

	return html;
}

void _handle_index( void) {
	bool reboot = false;
	if ( !g_followers_counter.server->authenticate( CONFIG_WERSERVER_USER, CONFIG_SERVERWEB_PASS)) {
		return g_followers_counter.server->requestAuthentication();
	}

	String message = "";
	if( g_followers_counter.server->arg("media_updated") == "true") {
		for( int media = 0; media < MEDIA_COUNT; media++) {
			String enable_media_str = "enabled_" + g_followers_counter.media_config[media].name;
			String duration_media_str = "duration_" + g_followers_counter.media_config[media].name;
			int duration = g_followers_counter.server->arg(duration_media_str).toInt();
			bool enabled = g_followers_counter.server->arg(enable_media_str) == "true";
			if( duration > 0) {
				g_followers_counter.media_config[media].display_duration_sec = duration;
				g_followers_counter.media_value[media].media_enabled = enabled;
				printf( "MEDIA %s - enable %d - duration %d\n", g_followers_counter.media_config[media].name.c_str(), g_followers_counter.media_value[media].media_enabled, g_followers_counter.media_config[media].display_duration_sec);

				_write_media_settings_to_eeprom( media);
			}
		}
		message = "Media settings changed.";
	} else if( g_followers_counter.server->arg("brightness") != "") {
		int newBrightness = g_followers_counter.server->arg("brightness").toInt();
		// La vraie limite max de brightness est 255, mais le courant nécessaire devient trop élevée pour un port USB
		if( newBrightness >= 0 && newBrightness <= 100) {
			g_followers_counter.led_brightness = newBrightness;

			// PVL Max value supported is 25. So to manage "real percentage" we will multiply value per 4 and divide just before set band brightness */
			g_followers_counter.band->setBrightness((int) (((float) (g_followers_counter.led_brightness) / 4.) + 0.5));
			g_followers_counter.band->show();

			_write_brightness_setting_to_eeprom();

			message = "Brightness changed.";
		}
	} else if( g_followers_counter.server->arg("fb_page_id") != "" && g_followers_counter.server->arg("fb_app_id") != "" && g_followers_counter.server->arg("fb_app_secret") != "" && g_followers_counter.server->arg("fb_access_token") != "") {
		g_followers_counter.server->arg("fb_page_id").toCharArray( g_followers_counter.api_info.fb_page_id, sizeof( g_followers_counter.api_info.fb_page_id));
		g_followers_counter.server->arg("fb_app_id").toCharArray( g_followers_counter.api_info.fb_app_id, sizeof( g_followers_counter.api_info.fb_app_id));
		g_followers_counter.server->arg("fb_app_secret").toCharArray( g_followers_counter.api_info.fb_app_secret, sizeof( g_followers_counter.api_info.fb_app_secret));
		g_followers_counter.server->arg("fb_access_token").toCharArray( g_followers_counter.api_info.fb_access_token, sizeof( g_followers_counter.api_info.fb_access_token));
		_write_api_settings_to_eeprom();
		reboot = true;
		message = "Facebook api settings changed. ! Counter will reboot !";
	} else if( g_followers_counter.server->arg("insta_page_name") != "") {
		g_followers_counter.server->arg("insta_page_name").toCharArray( g_followers_counter.api_info.insta_page_name, sizeof( g_followers_counter.api_info.insta_page_name));
		_write_api_settings_to_eeprom();
		reboot = true;
		message = "Instagram page name changed ! Counter will reboot !";
	} else if( g_followers_counter.server->arg("tw_page_name") != "") {
		g_followers_counter.server->arg("tw_page_name").toCharArray( g_followers_counter.api_info.tw_page_name, sizeof( g_followers_counter.api_info.tw_page_name));
		_write_api_settings_to_eeprom();
		reboot = true;
		message = "Twitter page name changed ! Counter will reboot !";
	} else if( g_followers_counter.server->arg("yt_channel_id") != "" && g_followers_counter.server->arg("yt_api_key") != "") {
		g_followers_counter.server->arg("yt_channel_id").toCharArray( g_followers_counter.api_info.yt_channel_id, sizeof( g_followers_counter.api_info.yt_channel_id));
		g_followers_counter.server->arg("yt_api_key").toCharArray( g_followers_counter.api_info.yt_api_key, sizeof( g_followers_counter.api_info.yt_api_key));
		_write_api_settings_to_eeprom();
		reboot = true;
		message = "Youtube api settings changed ! Counter will reboot !";
	}

	debug_printf( "message: %s\n", message.c_str());

	g_followers_counter.server->send( 200, "text/html", _generate_index_html( message));
	if( reboot == true) {
		_reboot();
	}
}

void _on_start( void) {
	String type;

	if( ArduinoOTA.getCommand() == U_FLASH) {
		type = F("sketch");
	} else {// U_SPIFFS
		type = F("filesystem");
	}

	// NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
	Serial.print( F("Start updating "));
	Serial.println( type);

	return;
}

void _on_end( void) {
	Serial.println(F("\nEnd"));

	return;
}

void _on_progress( unsigned int progress, unsigned int total) {
	Serial.printf("Progress: %u%%\r", (progress / (total / 100)));

	return;
}

void _on_error(ota_error_t error) {
	Serial.printf( "Error[%u]: ", error);
	switch( error) {
		case OTA_AUTH_ERROR:
			Serial.println( F("Auth Failed"));
			break;
		case OTA_BEGIN_ERROR:
			Serial.println( F("Begin Failed"));
			break;
		case OTA_CONNECT_ERROR:
			Serial.println( F("Connect Failed"));
			break;
		case OTA_RECEIVE_ERROR:
			Serial.println( F("Receive Failed"));
			break;
		case OTA_END_ERROR:
			Serial.println( F("End Failed"));
			break;
		default:
			Serial.println( F("Unknown error"));
			break;
	}

	return;
}

void _draw_ip_addr( IPAddress ip_addr, char r_value, char g_value, char b_value) {
	int digit;
	unsigned long number;

	#define STARTX 11
	#define STARTY 0

	if( g_followers_counter.unexpected_restart == true) {
		debug_println( "Undesired restart. Skip IP address");
		return;
	}

	debug_println( "Draw IP address");

	for( int ip_addr_part = 0; ip_addr_part < 2; ip_addr_part++) {
		number = (ip_addr[2*ip_addr_part] * 1000) + ip_addr[(2*ip_addr_part)+1];
		for( int i = 0; i < 6; i++) {
			digit = _get_digit( number, 6 - i);
			/* PVL : Ugly trick to hide useless '0' */
			switch( i) {
				case 0: if( ip_addr[2*ip_addr_part] < 100)     digit = -1; break;
				case 1: if( ip_addr[2*ip_addr_part] < 10)      digit = -1; break;
				case 3: if( ip_addr[(2*ip_addr_part)+1] < 100) digit = -1; break;
				case 4: if( ip_addr[(2*ip_addr_part)+1] < 10)  digit = -1; break;
			}
			_print_digit( digit, STARTX + i * 5, STARTY, r_value, g_value, b_value);
		}
		/* Draw '.' */
		g_followers_counter.pannel[7][25][0] = 255;
		g_followers_counter.pannel[7][25][1] = 255;
		g_followers_counter.pannel[7][25][2] = 255;
		if( ip_addr_part == 1) {
			g_followers_counter.pannel[7][10][0] = 255;
			g_followers_counter.pannel[7][10][1] = 255;
			g_followers_counter.pannel[7][10][2] = 255;
		}
		_refresh_display();
		delay(4000);
	}
}

void _print_media_logo_with_animation( int media) {
	switch( media) {
		case MEDIA_FACEBOOK:
			_print_logo_with_animation( g_fb_logo, g_fb_logo_colors);
			break;

		case MEDIA_INSTAGRAM:
			_print_logo_with_animation( g_insta_logo, g_insta_logo_colors);
			break;
		case MEDIA_TWITTER:
			_print_logo_with_animation( g_tw_logo, g_tw_logo_colors);
			break;

		case MEDIA_YOUTUBE:
			_print_logo_with_animation( g_yt_logo, g_yt_logo_colors);
			break;

		default:
			break;
	}

	return;
}

int _get_media_value( int media) {
	int value = -1;

	if( g_followers_counter.media_config[media].limit_call_per_hour != 0) {
		unsigned long minTimeBetweenCalls = 3600000 / g_followers_counter.media_config[media].limit_call_per_hour;
		if( ((millis() - g_followers_counter.media_value[media].last_call_ms) < minTimeBetweenCalls) && (g_followers_counter.media_value[media].first_call_done == true) && (g_followers_counter.media_value[media].in_error == false)) {
			value = g_followers_counter.media_value[media].last_value;
			printf( "\nSkipped %s call for API restriction. Last known value used\n", g_followers_counter.media_config[media].name.c_str());
			return value;
		}
	}

	printf( "\n%s API call\n", g_followers_counter.media_config[media].name.c_str());

	switch( media) {
		case MEDIA_FACEBOOK:
			value = _get_facebook_fan_count( g_followers_counter.api_info.fb_page_id);
			break;

		case MEDIA_INSTAGRAM:
			value = _get_instagram_follower_count( g_followers_counter.api_info.insta_page_name);
			break;

		case MEDIA_TWITTER:
			value = _get_twitter_follower_count( g_followers_counter.api_info.tw_page_name);
			break;

		case MEDIA_YOUTUBE:
			value = _get_youtube_subscriber_count( g_followers_counter.api_info.yt_channel_id);
			break;

		default:
			return -50;
			break;
	}
	printf( "%s: %d", g_followers_counter.media_config[media].name.c_str(), value);

	g_followers_counter.media_value[media].first_call_done = true;
	g_followers_counter.media_value[media].last_call_ms = millis();
	if( value >= 0) {
		g_followers_counter.media_value[media].last_value = value;
		g_followers_counter.media_value[media].in_error = false;
		g_followers_counter.media_value[media].obsolete_value = false;
	} else {
		if( g_followers_counter.media_value[media].last_value >= 0 && g_followers_counter.media_value[media].in_error == false) {
			value = g_followers_counter.media_value[media].last_value;
			g_followers_counter.media_value[media].in_error = false;
			g_followers_counter.media_value[media].obsolete_value = true;
		} else {
			g_followers_counter.media_value[media].last_value = 0;
			g_followers_counter.media_value[media].in_error = true;
			g_followers_counter.media_value[media].obsolete_value = false;
		}
	}
	printf( " (obsolete %d)\n", g_followers_counter.media_value[media].obsolete_value);

	_write_last_value_to_rtc( media);

	return value;
}

void _delay_with_handling( long ms) {
	unsigned long delayStartMillis = millis();

	while( millis() - delayStartMillis < ms) {
		delay(50);
		ArduinoOTA.handle();
		g_followers_counter.server->handleClient();
	}
}

void _setup_serial_com( void) {
	Serial.begin( 115200);
	delay( 10);
}

void _setup_gpio_management( void) {
	/* Reset */
	pinMode( RESET_PIN, INPUT_PULLUP);

	/* Debug */
	pinMode( DEBUG_PIN, INPUT_PULLUP);

	/* LED Test */
	pinMode( TEST_LED_PIN, INPUT_PULLUP);
	pinMode( FORCE_ALL_LED_MODE, INPUT_PULLUP);
}

void _setup_reset_info( void) {
	const rst_info * resetInfo = system_get_rst_info();

	debug_printf( "\nReset Reason: %d\n", resetInfo->reason);

	switch( resetInfo->reason){
		case 0: //power on ,vdd
		case 4: //soft restart
		case 6: //external restart
			g_followers_counter.unexpected_restart = false;
			break;

		case 1: //HW watchdog
		case 2: //Exception reset
		case 3: //SW watchdog
		case 5: //DeepSleep wakeup
		default:
			g_followers_counter.unexpected_restart = true;
	}

	return;
}

void _setup_eeprom( void) {
	EEPROM.begin( EEPROM_SIZE);
	debug_printf( "Initialize EEPROM: (size %d)\n", EEPROM_SIZE);

	if( EEPROM.read(0) == EEPROM_CHECK_VALUE && digitalRead( RESET_PIN) != 0) {
		debug_println( "Read settings from eeprom");
		_read_settings_from_eeprom();
	} else {
		debug_println( "Reset initial eeprom");
		/* Reset brightness */
		g_followers_counter.led_brightness = LED_BRIGHTNESS_INITIAL_PERCENTAGE;

		/* Reset media */
		for( int media = 0; media < MEDIA_COUNT; media++) {
			g_followers_counter.media_value[media].media_enabled = g_followers_counter.media_config[media].initial_status;
		}

		/* Reset api */
		memset( &g_followers_counter.api_info, '\0', sizeof( g_followers_counter.api_info));
		strncpy( g_followers_counter.api_info.fb_page_id, CONFIG_FACEBOOK_PAGE_ID, sizeof( g_followers_counter.api_info.fb_page_id));
		strncpy( g_followers_counter.api_info.fb_app_id, CONFIG_FACEBOOK_APP_ID, sizeof( g_followers_counter.api_info.fb_app_id));
		strncpy( g_followers_counter.api_info.fb_app_secret, CONFIG_FACEBOOK_APP_SECRET, sizeof( g_followers_counter.api_info.fb_app_secret));
		strncpy( g_followers_counter.api_info.fb_access_token, CONFIG_FACEBOOK_ACCESS_TOKEN, sizeof( g_followers_counter.api_info.fb_access_token));
		strncpy( g_followers_counter.api_info.insta_page_name, CONFIG_INSTAGRAM_PAGE_NAME, sizeof( g_followers_counter.api_info.insta_page_name));
		strncpy( g_followers_counter.api_info.tw_page_name, CONFIG_TWITTER_PAGE_NAME, sizeof( g_followers_counter.api_info.tw_page_name));
		strncpy( g_followers_counter.api_info.yt_channel_id, CONFIG_YOUTUBE_CHANNEL_ID, sizeof( g_followers_counter.api_info.yt_channel_id));
		strncpy( g_followers_counter.api_info.yt_api_key, CONFIG_YOUTUBE_API_KEY, sizeof( g_followers_counter.api_info.yt_api_key));

		_write_settings_to_eeprom();
	}

#if 0
	uint8_t value;
	debug_println("=============== EEPROM CONTENT ================")
	for( int idx = 0; idx < EEPROM_SIZE; idx++) {
		if( idx%32 == 0) debug_printf("\n%04X : ", idx);
		value = EEPROM.read(idx);
		debug_printf( "%02X ", value);
	}
	debug_println("\n\n=============== EEPROM CONTENT ================")
#endif

	_print_media_settings();
	_print_api_settings();
}

void _setup_band( void) {
	g_followers_counter.band = new Adafruit_NeoPixel( LED_NBR, LED_PIN, NEO_GRB + NEO_KHZ800);
	g_followers_counter.band->begin();

	// PVL Max value supported is 25. So to manage "real percentage" we will multiply value per 4 and divide just before set band brightness */
	g_followers_counter.band->setBrightness((int) (((float) (g_followers_counter.led_brightness) / 4.) + 0.5));
	g_followers_counter.band->clear();
}

void _setup_api( void) {
	/* Initialise client */
	g_followers_counter.client = new WiFiClientSecure;
	g_followers_counter.client->setTimeout( 15000);

	/* Initialise facebook api */
	g_followers_counter.facebookApi = new FacebookApi( *g_followers_counter.client, g_followers_counter.api_info.fb_access_token, g_followers_counter.api_info.fb_app_id, g_followers_counter.api_info.fb_app_secret);

	/* Initialise instagram api */
	g_followers_counter.instaStats = new InstagramStats( *g_followers_counter.client);
	g_followers_counter.client->setInsecure(); /* FOR INSTAGRAM: If using ESP8266 Core 2.5 RC */

	/* No api for twitter */

	/* Initialise youtube api */
	g_followers_counter.youtubeApi = new YoutubeApi( g_followers_counter.api_info.yt_api_key, *g_followers_counter.client);

	for( int media = 0; media < MEDIA_COUNT; media++) {
		g_followers_counter.media_value[media].last_call_ms = 0;
		g_followers_counter.media_value[media].last_value = 0;
		g_followers_counter.media_value[media].first_call_done = false;
		g_followers_counter.media_value[media].in_error = true;
		g_followers_counter.media_value[media].obsolete_value = false;
	}
}

void _setup_wifi( void) {
	IPAddress ip_addr;

	if( g_followers_counter.unexpected_restart == true) {
		int media;

		/* Launch first media */
		for( media = 0; media < MEDIA_COUNT; media++) {
			if( g_followers_counter.media_value[media].media_enabled) {
				_print_media_logo_with_animation( media);
				g_followers_counter.previous_number = g_followers_counter.media_value[media].last_value;
				if( _debugging() == true) {
					_print_6_digits_number( g_followers_counter.media_value[media].last_value, 11, 0, 100, 100, 255);
				} else {
					_print_6_digits_number( g_followers_counter.media_value[media].last_value, 11, 0, 255, 255, 255);
				}
				break;
			}
		}
		if( media >= MEDIA_COUNT) {
			_print_logo_with_animation( g_error_logo, g_error_logo_colors);
			_print_string( g_error_string);
		}
		_refresh_display();
	} else {
		_print_logo_with_animation( g_wifi_logo, g_wifi_logo_colors);
		_print_string( g_wifi_string);
		_refresh_display();
	}

	debug_println( "Initialise wifi connection");

	g_followers_counter.wifiMulti = new ESP8266WiFiMulti;

	debug_printf( "SSID1 : %s - PASS1 : %s\n", CONFIG_WIFI_SSID_1, CONFIG_WIFI_PASS_1);
	debug_printf( "SSID2 : %s - PASS2 : %s\n", CONFIG_WIFI_SSID_2, CONFIG_WIFI_PASS_2);
	debug_printf( "SSID3 : %s - PASS3 : %s\n", CONFIG_WIFI_SSID_3, CONFIG_WIFI_PASS_3);
	debug_printf( "SSID4 : %s - PASS4 : %s\n", CONFIG_WIFI_SSID_4, CONFIG_WIFI_PASS_4);
	debug_printf( "SSID5 : %s - PASS5 : %s\n", CONFIG_WIFI_SSID_5, CONFIG_WIFI_PASS_5);

	if( strcmp( CONFIG_WIFI_SSID_1, "") != 0) { g_followers_counter.wifiMulti->addAP( CONFIG_WIFI_SSID_1, CONFIG_WIFI_PASS_1, false, _debugging()); }
	if( strcmp( CONFIG_WIFI_SSID_2, "") != 0) { g_followers_counter.wifiMulti->addAP( CONFIG_WIFI_SSID_2, CONFIG_WIFI_PASS_2, true,  _debugging()); }
	if( strcmp( CONFIG_WIFI_SSID_3, "") != 0) { g_followers_counter.wifiMulti->addAP( CONFIG_WIFI_SSID_3, CONFIG_WIFI_PASS_3, false, _debugging()); }
	if( strcmp( CONFIG_WIFI_SSID_4, "") != 0) { g_followers_counter.wifiMulti->addAP( CONFIG_WIFI_SSID_4, CONFIG_WIFI_PASS_4, false, _debugging()); }
	if( strcmp( CONFIG_WIFI_SSID_5, "") != 0) { g_followers_counter.wifiMulti->addAP( CONFIG_WIFI_SSID_5, CONFIG_WIFI_PASS_5, false, _debugging()); }

	debug_println( "Connecting");

	while( g_followers_counter.wifiMulti->run( _debugging()) != WL_CONNECTED) {
		static int cpt = 0;
		delay( 500);
		cpt++;
		debug_print( ".");
		if( cpt >= 10) {
			debug_println( "");
			cpt = 0;
		}
		yield();
	}

	ip_addr = WiFi.localIP();

	debug_printf( "Connection success on: %s\n", WiFi.SSID().c_str());
	debug_printf( "IP address: %s\n", ip_addr.toString().c_str());
	debug_printf( "MAC address: %s\n", WiFi.macAddress().c_str());

	_draw_ip_addr( ip_addr, 0, 171, 255);

	return;
}

void _setup_server( void) {
	g_followers_counter.server = new ESP8266WebServer(80);

	g_followers_counter.server->on("/index", _handle_index);
	g_followers_counter.server->begin();
}

void _setup_ota( void) {
	ArduinoOTA.onStart( _on_start);
	ArduinoOTA.onEnd( _on_end);
	ArduinoOTA.onProgress( _on_progress);
	ArduinoOTA.onError( _on_error);
	ArduinoOTA.setPassword( CONFIG_OTA_PASS);
	ArduinoOTA.begin();
}

void setup( void) {
	int media;
	memset( &g_followers_counter.api_info, '\0', sizeof( g_followers_counter.api_info));

	/* Init
	 * Warning init order is very important
	 */
	_setup_serial_com();
	_setup_gpio_management();
	_setup_reset_info();
	debug_printf( "\nSoftware version %s\n", VERSION);
	_pin_monitor();
	_setup_eeprom();
	_setup_band();
	_setup_api();
	_read_last_values_from_rtc();
	_setup_wifi();
	_setup_ota();
	_setup_server();
	_perf_monitor();

	/* Launch first media */
	for( media = 0; media < MEDIA_COUNT; media++) {
		if( g_followers_counter.media_value[media].media_enabled) {
			_print_media_logo_with_animation( media);
			g_followers_counter.previous_number = g_followers_counter.media_value[media].last_value;
			if( _debugging() == true) {
				_print_6_digits_number( g_followers_counter.media_value[media].last_value, 11, 0, 100, 100, 255);
			} else {
				_print_6_digits_number( g_followers_counter.media_value[media].last_value, 11, 0, 255, 255, 255);
			}
			break;
		}
	}
	if( media >= MEDIA_COUNT) {
		_print_logo_with_animation( g_error_logo, g_error_logo_colors);
		_print_string( g_error_string);
	}
	_refresh_display();
}

void loop( void) {
	static bool in_error = false;
	unsigned int display_duration_sec;
	ArduinoOTA.handle();
	g_followers_counter.server->handleClient();

	_perf_monitor();

	int media_enabled_count = 0;
	for( int media = 0; media < MEDIA_COUNT; media++) {
		media_enabled_count += g_followers_counter.media_value[media].media_enabled;
		if( !g_followers_counter.media_value[media].media_enabled) {
			continue; // Le média n'est pas activé
		}

		bool logo_displayed = ((g_followers_counter.previous_media == media) && (in_error == false));

		g_followers_counter.previous_media = media;

		unsigned long media_start_millis = millis();
		unsigned long last_refresh_millis = 0;
		bool first_call_in_a_loop = true;

		display_duration_sec = g_followers_counter.media_config[media].display_duration_sec;

		while( millis() - media_start_millis < display_duration_sec * 1000) {
			if( !g_followers_counter.media_value[media].media_enabled) {
				break; // Le média n'est pas activé
			}

			if( millis() - last_refresh_millis >= REFRESH_INTERVAL_MS) {
				last_refresh_millis = millis();

				int value = _get_media_value( media);

				if( first_call_in_a_loop == true) {
					media_start_millis = millis();
					first_call_in_a_loop = false;
				}

				if( !logo_displayed) {
					_print_media_logo_with_animation( media);
				}
				if( value >= 0) {
					/* Display number in light blue when value is obsolete :) */
					if( g_followers_counter.media_value[media].obsolete_value == true && _debugging() == true) {
						_print_6_digits_number_with_animation( value, 11, 0, 100, 100, 255, !logo_displayed);
					} else {
						_print_6_digits_number_with_animation( value, 11, 0, 255, 255, 255, !logo_displayed);
					}
				} else {
					_print_string( g_error_string);
				}

				logo_displayed = true;
				if( value >= 0) {
					g_followers_counter.previous_number = value;
				} else {
					g_followers_counter.previous_number = 0;
				}

				_refresh_display();
				_perf_monitor();
				_led_keep_alive();
			}
			_delay_with_handling( 50);
		}
	}
	if( media_enabled_count <= 0) {
		in_error = true;
		debug_println( "ERROR NO MEDIA");
		_print_logo_with_animation( g_error_logo, g_error_logo_colors);
		_print_string( g_no_media_string);
		_refresh_display();
		_led_keep_alive();
		delay(50);
	} else {
		in_error = false;
	}
}
